﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Mediterrum.DatabaseTools
{
	public class DataTableTool
	{
		#region Properties
		/// <summary>
		/// Selection of styles for column names (TitleCase, lowerCamelCase, UPPER_CASE_UNDERSCORE, LOWER_CASE_UNDERSCORE)
		/// </summary>
		public enum ColumnNameStyles
		{
			TitleCase,
			LowerCamelCase,
			UpperCaseUnderscore,
			LowerCaseUnderscore
		}

		/// <summary>
		/// Prefix for input parameter names
		/// </summary>
		public static string InputPrefix { get; set; } = "I_";
		/// <summary>
		/// Prefix for output parameter names
		/// </summary>
		public static string OutputPrefix { get; set; } = "O_";
		/// <summary>
		/// Prefix for input/output parameter names
		/// </summary>
		public static string InputOutputPrefix { get; set; } = "IO_";
		/// <summary>
		/// Are denifed prefixes stripped from column names when converting into property names
		/// </summary>
		public static bool StripPrefixes { get; set; } = true;
		/// <summary>
		/// Used column name style (TitleCase, lowerCamelCase, UPPER_CASE_UNDERSCORE, LOWER_CASE_UNDERSCORE)
		/// </summary>
		public static ColumnNameStyles ColumnNameStyle { get; set; } = ColumnNameStyles.UpperCaseUnderscore;
		#endregion

		#region Methods
		/// <summary>
		/// Converts a DataTable into a dictionary, one field per key
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="keyIndex">Index of column for dictionary keys</param>
		/// <param name="valueIndex">Index of column for dictionary values</param>
		public static Dictionary<K, V> ToDictionary<K, V>(DataTable dataTable, int keyIndex, int valueIndex)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.ToDictionary(
					row => (K)ReadValue<K>(row[keyIndex]),
					row => (V)ReadValue<V>(row[valueIndex])
				);
		} // ToDictionary<K, V>(DataTable dataTable, int keyIndex, int valueIndex)

		/// <summary>
		/// Converts a DataTable into a dictionary, one field per key
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="keyColumn">Name of column for dictionary keys</param>
		/// <param name="valueColumn">Name of column for dictionary values</param>
		public static Dictionary<K, V> ToDictionary<K, V>(DataTable dataTable, string keyColumn, string valueColumn)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.ToDictionary(
					row => (K)ReadValue<K>(row[keyColumn]),
					row => (V)ReadValue<V>(row[valueColumn])
				);
		} // ToDictionary<K, V>(DataTable dataTable, string keyColumn, string valueColumn)

		/// <summary>
		/// Converts a DataTable into a dictionary, one row as class per key
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="keyIndex">Index of column for dictionary keys</param>
		public static Dictionary<K, V> ToDictionary<K, V>(DataTable dataTable, int keyIndex) where V : new()
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.ToDictionary(
					row => (K)ReadValue<K>(row[keyIndex]),
					row => ToClass<V>(row)
				);
		} // ToDictionary<K, V>(DataTable dataTable, int keyIndex)

		/// <summary>
		/// Converts a DataTable into a dictionary, one row as class per key
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="keyColumn">Name of column for dictionary keys</param>
		public static Dictionary<K, V> ToDictionary<K, V>(DataTable dataTable, string keyColumn) where V : new()
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.ToDictionary(
					row => (K)ReadValue<K>(row[keyColumn]),
					row => ToClass<V>(row)
				);
		} // ToDictionary<K, V>(DataTable dataTable, string keyColumn)

		/// <summary>
		/// Converts a DataTable into a list
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		public static List<T> ToList<T>(DataTable dataTable) where T : new()
		{
			return dataTable
				.AsEnumerable()
				.Select(row => ToClass<T>(row))
				.ToList();
		} // ToList<T>(DataTable dataTable)

		/// <summary>
		/// Converts a column of data from a DataTable into a list
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="index">Index of column for list data</param>
		public static List<T> ToList<T>(DataTable dataTable, int index)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.Select(row => (T)ReadValue<T>(row[index]))
				.ToList();
		} // ToList<T>(DataTable dataTable, int index)

		/// <summary>
		/// Converts a column of data from a DataTable into a list
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="column">Name of column for list data</param>
		public static List<T> ToList<T>(DataTable dataTable, string column)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.Select(row => (T)ReadValue<T>(row[column])) 
				.ToList();
		} // ToList<T>(DataTable dataTable, string column)

		/// <summary>
		/// Converts a column of data from a DataTable into a list of key-value pairs
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="keyIndex">Index of column used for keys</param>
		/// <param name="valueIndex">Index of column used for values</param>
		public static List<KeyValuePair<K, V>> ToList<K, V>(DataTable dataTable, int keyIndex, int valueIndex)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.Select(row =>
					new KeyValuePair<K, V>(
						(K)ReadValue<K>(row[keyIndex]),
						(V)ReadValue<V>(row[valueIndex])
					)
				)
				.ToList();
		} // ToList<K, V>(DataTable dataTable, int keyIndex, int valueIndex)

		/// <summary>
		/// Converts a column of data from a DataTable into a list of key-value pairs
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="keyColumn">Name of column used for keys</param>
		/// <param name="valueColumn">Name of column used for values</param>
		public static List<KeyValuePair<K, V>> ToList<K, V>(DataTable dataTable, string keyColumn, string valueColumn)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.Select(row =>
					new KeyValuePair<K, V>(
						(K)ReadValue<K>(row[keyColumn]),
						(V)ReadValue<V>(row[valueColumn])
					)
				)
				.ToList();
		} // ToList<K, V>(DataTable dataTable, string keyColumn, string valueColumn)

		/// <summary>
		/// Converts a DataTable into an array of classes
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		public static T[] ToArray<T>(DataTable dataTable) where T : new()
		{
			return dataTable
				.AsEnumerable()
				.Select(row => ToClass<T>(row))
				.ToArray();
		} // ToArray<T>(DataTable dataTable)

		/// <summary>
		/// Converts a DataTable into an array
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="index">Index of column used for values</param>
		public static T[] ToArray<T>(DataTable dataTable, int index)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.Select(row => (T)ReadValue<T>(row[index]))
				.ToArray();
		} // ToArray<T>(DataTable dataTable, int index)

		/// <summary>
		/// Converts a DataTable into an array
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="column">Name of column used for values</param>
		public static T[] ToArray<T>(DataTable dataTable, string column)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.Select(row => (T)ReadValue<T>(row[column]))
				.ToArray();
		} // ToArray<T>(DataTable dataTable, string column)

		/// <summary>
		/// Converts a DataTable into an array of key-value pairs
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="keyIndex">Index of column used for keys</param>
		/// <param name="valueIndex">Index of column used for values</param>
		public static KeyValuePair<K, V>[] ToArray<K, V>(DataTable dataTable, int keyIndex, int valueIndex)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.Select(row =>
					new KeyValuePair<K, V>(
						(K)ReadValue<K>(row[keyIndex]),
						(V)ReadValue<V>(row[valueIndex])
					)
				)
				.ToArray();
		} // ToArray<K, V>(DataTable dataTable, int keyIndex, int valueIndex)

		/// <summary>
		/// Converts a DataTable into an array of key-value pairs
		/// </summary>
		/// <param name="dataTable">Source data table</param>
		/// <param name="keyColumn">Name of column used for keys</param>
		/// <param name="valueColumn">Name of column used for values</param>
		public static KeyValuePair<K, V>[] ToArray<K, V>(DataTable dataTable, string keyColumn, string valueColumn)
		{
			return dataTable
				.Select(null, null, DataViewRowState.OriginalRows)
				.Select(row =>
					new KeyValuePair<K, V>(
						(K)ReadValue<K>(row[keyColumn]),
						(V)ReadValue<V>(row[valueColumn])
					)
				)
				.ToArray();
		} // ToArray<K, V>(DataTable dataTable, string keyColumn, string valueColumn)

		/// <summary>
		/// Converts a DataRow into a class
		/// </summary>
		/// <param name="dataRow">Source data row</param>
		public static T ToClass<T>(DataRow dataRow) where T : new()
		{
			T output = new T();
			Type outputType = output.GetType();
			TypedReference refOutput = __makeref(output);

			foreach (FieldInfo field in outputType.GetFields())
			{
				string name = ToDatabaseName(field.Name);

				if (
					!dataRow.Table.Columns.Contains(name) 
					&& (
						!StripPrefixes
						|| (
							(name = FindColumn(dataRow.Table.Columns, InputPrefix + name)) == null
							&& (name = FindColumn(dataRow.Table.Columns, InputOutputPrefix + name)) == null
						)
					)
				)
				{
					continue;
				}

				if (ReadValue(field.FieldType, dataRow[name], out object value))
				{
					field.SetValueDirect(refOutput, value);
				}
			}

			foreach (PropertyInfo property in outputType.GetProperties())
			{
				if (!property.CanWrite)
				{
					continue;
				}

				string name = ToDatabaseName(property.Name);

				if (
					!dataRow.Table.Columns.Contains(name)
					&& (
						!StripPrefixes
						|| (
							(name = FindColumn(dataRow.Table.Columns, InputPrefix + name)) == null
							&& (name = FindColumn(dataRow.Table.Columns, InputOutputPrefix + name)) == null
						)
					)
				)
				{
					continue;
				}

				if (ReadValue(property.PropertyType, dataRow[name], out object value))
				{
					property.SetValue(output, value);
				}
			}

			return output;
		} // ToClass<T>(DataRow dataRow)

		/// <summary>
		/// Converts an array of classes into a DataTable
		/// </summary>
		/// <param name="dataList">Source data</param>
		public static DataTable ToDataTable<T>(List<T> dataList) where T : class, new()
		{
			return ToDataTable(dataList.ToArray());
		}

		/// <summary>
		/// Converts an IEnumerable of classes into a DataTable
		/// </summary>
		/// <param name="dataList">Source data</param>
		public static DataTable ToDataTable<T>(IEnumerable<T> dataList) where T : class, new()
		{
			return ToDataTable(dataList.ToArray());
		}

		/// <summary>
		/// Converts an array of classes into a DataTable
		/// </summary>
		/// <param name="dataList">Source data</param>
		public static DataTable ToDataTable<T>(T[] dataList) where T : class, new()
		{
			DataTable dataTable = new DataTable();
			List<KeyValuePair<string, object>> classMap = new List<KeyValuePair<string, object>>();
			Type type = typeof(T);

			// Read fields
			foreach (FieldInfo field in type.GetFields())
			{
				classMap.Add(new KeyValuePair<string, object>(field.Name, field));

				if (field.FieldType.IsGenericType && field.FieldType.GetGenericTypeDefinition() == typeof(Nullable<>))
				{
					dataTable.Columns.Add(ToDatabaseName(field.Name), Nullable.GetUnderlyingType(field.FieldType));
				}
				else
				{
					dataTable.Columns.Add(ToDatabaseName(field.Name), field.FieldType);
				}
			}

			// Read readable properties
			foreach (PropertyInfo property in type.GetProperties())
			{
				if (!property.CanRead)
				{
					continue;
				}

				classMap.Add(new KeyValuePair<string, object>(property.Name, property));

				if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
				{
					dataTable.Columns.Add(ToDatabaseName(property.Name), Nullable.GetUnderlyingType(property.PropertyType));
				}
				else
				{
					dataTable.Columns.Add(ToDatabaseName(property.Name), property.PropertyType);
				}
			}

			for (int r = 0; r < dataList.Length; r++)
			{
				object[] data = new object[dataTable.Columns.Count];
				T item = dataList[r];
				TypedReference refItem = __makeref(item);

				for (int c = 0; c < classMap.Count; c++)
				{
					KeyValuePair<string, object> column = classMap[c];

					if (column.Value is FieldInfo)
					{
						data[c] = ((FieldInfo)column.Value).GetValueDirect(refItem) ?? DBNull.Value;
					}
					else
					{
						data[c] = ((PropertyInfo)column.Value).GetValue(item) ?? DBNull.Value;
					}
				}

				dataTable.Rows.Add(data);
			}

			return dataTable;
		} // ToDataTable<T>(T[] dataList)

		/// <summary>
		/// Maps a class into a dictinary of property names and data types
		/// </summary>
		/// <param name="from">Source object</param>
		/// <param name="direction">Read-write status of properties to be mapped</param>
		public static Dictionary<string, Type> MapClass<T>(T from, ParameterDirection direction = ParameterDirection.InputOutput) where T : class
		{
			Dictionary<string, Type> output = new Dictionary<string, Type>();
			Type type = typeof(T);

			foreach (FieldInfo field in type.GetFields())
			{
				output.Add(ToDatabaseName(field.Name), field.FieldType);
			}

			foreach (PropertyInfo property in type.GetProperties())
			{
				if (!property.CanRead && direction != ParameterDirection.Input)
				{
					continue;
				}
				if (!property.CanWrite && direction != ParameterDirection.Output)
				{
					continue;
				}

				output.Add(ToDatabaseName(property.Name), property.PropertyType);
			}

			return output;
		}

		#endregion

		#region Private methods
		protected static string StripPrefix(string columnName)
		{
			string columnNameUC = columnName.ToUpper();

			if (StripPrefixes)
			{
				if (columnNameUC.StartsWith(InputPrefix))
				{
					return columnName.Substring(InputPrefix.Length);
				}
				if (columnNameUC.StartsWith(OutputPrefix))
				{
					return columnName.Substring(OutputPrefix.Length);
				}
				if (columnNameUC.StartsWith(InputOutputPrefix))
				{
					return columnName.Substring(InputOutputPrefix.Length);
				}
			}
			return columnNameUC;
		} // StripPrefix(string columnName)

		private static string FindColumn(DataColumnCollection columns, string name)
		{
			if (columns.Contains(name))
			{
				return name;
			}
			return null;
		}

		/// <summary>
		/// Converts a database column name into a class property name based on ColumnNameStyle setting
		/// </summary>
		/// <param name="columnName">Column name</param>
		public static string ToClassName(string columnName)
		{
			switch (ColumnNameStyle)
			{
				case ColumnNameStyles.UpperCaseUnderscore:
				case ColumnNameStyles.LowerCaseUnderscore:
					return string.Join(
						"",
						StripPrefix(columnName)
							.Split('_')
							.Select((w) =>
								w.Substring(0, 1).ToUpper()
								+ w.Substring(1).ToLower()
							)
					);
				case ColumnNameStyles.LowerCamelCase:
					return
						columnName.Substring(0, 1).ToUpper()
						+ columnName.Substring(1).ToLower();
				default:
					return columnName;
			}
		} // ToClassName()

		/// <summary>
		/// Converts a class property name into a database column based on ColumnNameStyle setting
		/// </summary>
		/// <param name="columnName">Column name</param>
		public static string ToDatabaseName(string className)
		{
			switch (ColumnNameStyle)
			{
				case ColumnNameStyles.UpperCaseUnderscore:
					return string.Join(
						"",
						className.ToCharArray()
							.Select((c, i) =>
								i > 0 && Char.IsUpper(c) ? "_" + c : c.ToString()
							)
					).ToUpper();
				case ColumnNameStyles.LowerCaseUnderscore:
					return string.Join(
						"",
						className.ToCharArray()
							.Select((c, i) =>
								i > 0 && Char.IsUpper(c) ? "_" + c : c.ToString()
							)
					).ToLower();
				case ColumnNameStyles.LowerCamelCase:
					return
						className.Substring(0, 1).ToUpper()
						+ className.Substring(1).ToLower();
				default:
					return className;
			}
		} // DeCamelcase(string input)

		private static object ReadValue(object input, string name)
		{
			Type type = input.GetType();
			TypedReference refInput = __makeref(input);

			foreach (FieldInfo field in type.GetFields())
			{
				if (field.Name == name)
				{
					return field.GetValueDirect(refInput);
				}
			}

			foreach (PropertyInfo property in type.GetProperties())
			{
				if (property.Name == name)
				{
					if (!property.CanRead)
					{
						return null;
					}
					return property.GetValue(input);
				}
			}

			return null;
		} // ReadValue(object input, string name)

		private static object ReadValue<T>(object input)
		{
			Type type = typeof(T);

			if (ReadValue(type, input, out object output))
			{
				if (output == null)
				{
					return default(T);
				}
				return output;
			}
			else
			{
				return default(T);
			}
		} // ReadValue<T>(object input)

		private static bool ReadValue(Type type, object input, out object output)
		{
			if (input == DBNull.Value)
			{
				output = null;
				return true;
			}
			else if (type == typeof(bool) || type == typeof(bool?))
			{
				Type typeIn = input.GetType();

				if (typeIn == typeof(decimal))
				{
					output = ((decimal)input != 0);
				}
				else
				{
					output = (Convert.ToInt32(input) != 0);
				}
				return true;
			}
			else if (type == typeof(float) || type == typeof(float?))
			{
				if (input.GetType() == typeof(double))
				{
					output = (float)(double)input;
				}
				else
				{
					output = Convert.ToSingle(input);
				}
				return true;
			}
			else if (type == typeof(double) || type == typeof(double?))
			{
				if (input.GetType() == typeof(double))
				{
					output = (double)input;
				}
				else
				{
					output = Convert.ToDouble(input);
				}
				return true;
			}
			else if (type == typeof(decimal) || type == typeof(decimal?))
			{
				if (input.GetType() == typeof(double))
				{
					output = (decimal)(double)input;
				}
				else
				{
					output = Convert.ToDecimal(input);
				}
				return true;
			}
			else if (type == typeof(short) || type == typeof(short?))
			{
				if (input.GetType() == typeof(decimal))
				{
					output = (short)(decimal)input;
				}
				else
				{
					output = Convert.ToInt16(input);
				}
				return true;
			}
			else if (type == typeof(int) || type == typeof(int?))
			{
				if (input.GetType() == typeof(decimal))
				{
					output = (int)(decimal)input;
				}
				else
				{
					output = Convert.ToInt32(input);
				}
				return true;
			}
			else if (type == typeof(long) || type == typeof(long?))
			{
				if (input.GetType() == typeof(decimal))
				{
					output = (long)(decimal)input;
				}
				else
				{
					output = Convert.ToInt64(input);
				}
				return true;
			}
			else if (type == typeof(string))
			{
				output = (string)input;
				return true;
			}
			else if (type == typeof(DateTime) || type == typeof(DateTime?))
			{
				output = (DateTime?)input;
				return true;
			}
			else if (type == typeof(byte[]))
			{
				output = (byte[])input;
				return true;
			}
			else
			{
				output = null;
				return false;
			}
		} // ReadValue(Type type, object input, out object output)

		#endregion
	}
}